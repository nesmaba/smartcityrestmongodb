var Contaminacion = require("../models/contaminacion.model");

exports.showAll = function (req, res) {
    Contaminacion.find({}, function(err, result) {
        if (err) throw err;
        
        console.log(result);
        res.send(result);
    });
}

exports.create = function (req, res) {
    
    let contaminacion = new Contaminacion( // let indica variable local de ámbito todo el bloque
        {
            fecha: req.body.fecha,
            hora: req.body.hora,
            ppm: req.body.ppm,
            latitud: req.body.latitud,
            longitud: req.body.longitud,
            temperatura: req.body.temperatura,
            humedad: req.body.humedad
        }
    );

    contaminacion.save(function (err) {
        if (err) {
            return next(err); // Si la solicitud actual no finaliza, next() pasa el control a la siguiente función de middleware. De lo contrario, la solicitud quedará colgada.
        }
        res.send('Contaminacion Created successfully')
    })
}

exports.showToday = function (req, res) { // https://mongoosejs.com/docs/queries.html
    var dat= new Date();  //Obtienes la fecha
    var fecha = dat.getDate()+"/"+(dat.getMonth()+1)+"/"+dat.getFullYear();
    Contaminacion.find({}).
    where('fecha').equals(fecha).
    select('latitud longitud ppm temperatura humedad').
    exec(function(err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);
    });
}

/*
// Using query builder
Person.
  find({ occupation: /host/ }).
  where('name.last').equals('Ghost').
  where('age').gt(17).lt(66).
  where('likes').in(['vaporizing', 'talking']).
  limit(10).
  sort('-occupation').
  select('name occupation').
  exec(callback);
*/