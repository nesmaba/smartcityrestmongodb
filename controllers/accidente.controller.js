var Accidente = require("../models/accidente.model");

exports.showAll = function (req, res) {
    Accidente.find({}, function(err, result) {
        if (err) throw err;
        
        console.log(result);
        res.send(result);
    });
}

exports.create = function (req, res) {
    
    let accidente = new Accidente( // let indica variable local de ámbito todo el bloque
        {
            fecha: req.body.fecha,
            hora: req.body.hora,
            latitud: req.body.latitud,
            longitud: req.body.longitud,
            matricula: req.body.matricula,
            atendido: req.body.atendido
        }
    );

    accidente.save(function (err) {
        if (err) {
            return next(err); // Si la solicitud actual no finaliza, next() pasa el control a la siguiente función de middleware. De lo contrario, la solicitud quedará colgada.
        }
        res.send('Accidente Created successfully')
    })
}

exports.showToday = function (req, res) { // https://mongoosejs.com/docs/queries.html
    var dat= new Date();  //Obtienes la fecha
    var fecha = dat.getDate()+"/"+(dat.getMonth()+1)+"/"+dat.getFullYear();
    Accidente.find({}).
    where('fecha').equals(fecha).
    where('atendido').equals(false).
    select('latitud longitud').
    exec(function(err, result) {
        if (err) throw err;
        
        console.log(result);
        res.send(result);
    });
}

/*
// Using query builder
Person.
  find({ occupation: /host/ }).
  where('name.last').equals('Ghost').
  where('age').gt(17).lt(66).
  where('likes').in(['vaporizing', 'talking']).
  limit(10).
  sort('-occupation').
  select('name occupation').
  exec(callback);
*/