var express = require('express'); // Importamos módulo express
var app = express(); // Creamos una app express

var accidente = require('./routes/accidente.route');
var contaminacion = require('./routes/contaminacion.route');

var mongoose = require('mongoose');
const bodyParser = require('body-parser');

// Nos conectamos a mongoDB, al esquema smartcity
mongoose.connect('mongodb://localhost/smartcity', { useNewUrlParser: true }, function(error){
   if(error){
      throw error; 
   }else{
      console.log('Conectado a MongoDB');
   }
});

mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Declaramos las rutas
app.use('/accidentes',  accidente); // Para la ruta /accidentes haz uso del route accidente.route.js
app.use('/contaminacion',  contaminacion); // Para la ruta /contaminacion haz uso del route accidente.route.js

app.listen(3000, function() {
    console.log('Aplicación ejemplo, escuchando el puerto 3000!');
  });