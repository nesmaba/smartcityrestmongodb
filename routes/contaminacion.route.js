var express = require('express');
var router = express.Router();
const contaminacionController = require('../controllers/contaminacion.controller');

// Contaminacion de aire en la SmartCity
router.get('/', contaminacionController.showAll); //Usamos una función que está en la clase controlador accidente.controller.js
router.post('/create', contaminacionController.create); // Usamos esta funcion para crear un nuevo accidente
router.get('/hoy', contaminacionController.showToday); //Usamos una función que está en la clase controlador accidente.controller.js

module.exports = router;