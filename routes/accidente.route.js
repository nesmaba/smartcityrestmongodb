var express = require('express');
var router = express.Router();
const accidenteController = require('../controllers/accidente.controller');

// Accidentes de tráfico en la SmartCity
router.get('/', accidenteController.showAll); //Usamos una función que está en la clase controlador accidente.controller.js
router.post('/create', accidenteController.create); // Usamos esta funcion para crear un nuevo accidente
router.get('/hoy', accidenteController.showToday); //Usamos una función que está en la clase controlador accidente.controller.js

/* Otra forma de hacerlo sin separar el controller del route
router.get('/', function(req, res) { // Muestra todos los accidentes
    // var dat= new Date();  //Obtienes la fecha
    // var fechaFormat = dat.getDate()+"/"+(dat.getMonth()+1)+"/"+dat.getFullYear();
    
    var fecha = req.query.fecha;
    var atendido = req.query.atendido;

    if (typeof fecha != 'undefined' && typeof atendido != 'undefined'){
        console.log("GET "+fecha);
        console.log("GET: "+atendido);
            
        Accidente.find({ fecha: fecha,
            atendido: atendido}, function(err, result) {
            if (err) throw err;
            console.log(result);
            res.send(result);
        });
    }
     
    Accidente.find({}, function(err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);
    });
});
*/
/*
router.get('/:fecha/:atendido', function(req, res) { // Muestra todos los accidentes
    var fecha = req.params.fecha; // Los params son los separados por /
    var atendido = req.params.atendido;

    console.log("Fecha del get "+fecha);
    console.log("Atendido del get "+atendido);
    var dat= new Date();  //Obtienes la fecha
    var fechaFormat = dat.getDate()+"/"+(dat.getMonth()+1)+"/"+dat.getFullYear();
    
    console.log(fechaFormat);
          
    Accidente.find({fecha:fecha,
        atendido: atendido}, function(err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);
      });
});

router.post('/', function(req, res) {
    res.send('POST handler for /accidentes route.');
});
*/
module.exports = router;