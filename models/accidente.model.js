var mongoose = require('mongoose');

var AccidenteSchema = mongoose.Schema({
    fecha: {type: String, required: true}, // Comviene cambiarlo a Date
    hora: {type: String, required: true},
    latitud: {type: Number, required: true},
    longitud: {type: Number, required: true},
    matricula: {type: String, required: true},
    atendido: {type: Boolean, required: true}
 });

 // Export the model
 module.exports = mongoose.model("Accidente", AccidenteSchema);