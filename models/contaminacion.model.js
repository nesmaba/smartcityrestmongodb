var mongoose = require('mongoose');

var ContaminacionSchema = mongoose.Schema({
    fecha: {type: String, required: true}, // Comviene cambiarlo a Date
    hora: {type: String, required: true},
    ppm: {type: Number, required: true},
    latitud: {type: Number, required: true},
    longitud: {type: Number, required: true},
    temperatura: {type: Number, required: true},
    humedad: {type: Number, required: true}
 });

 // Export the model
 module.exports = mongoose.model("Contaminacion", ContaminacionSchema);